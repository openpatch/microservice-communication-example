# [WIP] Mircoservice Communication Example

> Hopefully I will finish this example in the next week. Stay tuned :)

This example shows how microservices can communicate. Either via http or via amqp.

```plantuml
@startuml
() "HTTP" as Service2HTTP
() "HTTP" as Service3HTTP

() "AMQP" as RabbitMQTT
queue RabbitMQ

actor User as u

[Service1] --> RabbitMQTT

Service2HTTP -right- [Service2]
Service3HTTP -left- [Service3]
[Service2] --> RabbitMQTT
[Service3] --> Service2HTTP
u -left-> Service3HTTP


RabbitMQTT -down- [RabbitMQ]

@enduml
```
