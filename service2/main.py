import os
import requests
from flask import Flask, request, jsonify

app = Flask(__name__)

processed = []


@app.route("/", methods=["GET", "POST"])
def root():
    if request.method == "GET":
        return jsonify(processed)
    elif request.method == "POST":
        # publish message
        return jsonify({"msg": "Is processing. Come back later."})
