import os
import requests
from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def root():
    service2 = os.getenv("SERVICE_2")
    if request.method == "GET":
        # get the processed stuff
        response = requests.get(service2)
        return jsonify(processed)
    elif request.method == "POST":
        # asynchronous task
        requests.post(service2)
        return jsonify({"msg": "will be processed by service 2 and service 1"})
